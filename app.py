from flask import Flask, request, jsonify
import mysql.connector
import os
from dotenv import load_dotenv

app = Flask(__name__)

# Load environment variables from .env file
load_dotenv()

# Database configuration
HOST = os.getenv('DB_HOST')
USERNAME = os.getenv('DB_USERNAME')
PASSWORD = os.getenv('DB_PASSWORD')
DATABASE = os.getenv('DB_DATABASE')

# Database functions
def connect_db():
    """Connects to the database and returns a connection object"""
    try:
        connection = mysql.connector.connect(
            host=HOST, user=USERNAME, password=PASSWORD, database=DATABASE
        )
        return connection
    except mysql.connector.Error as err:
        print(f"Error connecting to database: {err}")
        return None

# Flask routes
@app.route('/')
def get_reversed_ip():
    
    """Returns both original and reversed IP addresses"""
    original_ip = request.remote_addr
    reversed_ip = '.'.join(original_ip.split('.')[::-1])

    try:
        connection = connect_db()
        if connection:
            cursor = connection.cursor()
            
            # Creating the table if it doesn't exist
            table = """
            CREATE TABLE IF NOT EXISTS address (
                id INT AUTO_INCREMENT PRIMARY KEY,
                ip VARCHAR(15)
            )
            """
            cursor.execute(table)
            print("Table creation query executed.")
            
            # Inserting the reversed IP into the table
            sql = "INSERT INTO address (ip) VALUES (%s)"
            cursor.execute(sql, (reversed_ip,))
            print("Insert query executed.")
            
            # Committing the transaction
            connection.commit()
            print("Successfully inserted IP data")
            
            cursor.close()
            
            connection.close()
    except mysql.connector.Error as err:
        print(f"Error inserting data: {err}")
    
    return jsonify({'original_ip': original_ip, 'reversed_ip': reversed_ip})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=3000)

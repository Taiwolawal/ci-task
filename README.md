# CI-task

The provided code [app.py](https://gitlab.com/Taiwolawal/ci-task/-/blob/main/app.py?ref_type=heads) implements a Flask web application that captures the client's public IP address, reverses it, and stores the reversed IP in a MySQL database. It also ensures the necessary database table exists before performing the insert operation.

## Setting up the CICD pipeline

The CICD pipeline is splitted in CI and CD respectively:

- CI Pipeline: It consist of 2 stages (build_and_push and deploy_pipeline) The dockerfile used to build the image used multi-stage build which ensured the images was not too heavy and after building the image successfully and pushing to docker repository, this triggered the next stage which is the CD pipeline which activates the CD pipeline.

![alt text](image-1.png)



<img src="images/image-39.png">

- CD Pipeline: The `deploy-dev` stage in the CD pipeline updates the container image  in the `carbon-app.yml`, which consist of the manifest file  having deployment, service and ingress which will be used to deploy application into the kubernetes cluster. The link to the [CD Pipeline](https://gitlab.com/Taiwolawal/cd-task)

<img src="images/image-41.png">


Run the pipeline


<img src="images/image-22.png">


<img src="images/image-23.png">


Provisioned a Kubernetes Cluster with a terraform  [code](https://github.com/Taiwolawal/k8s-eks) including: 
- ArgoCD: Following GitOps approach of deploying our applications into the Kubernetes Cluster using git as single source of truth.
- Ingress controller: To provision load balancer which will provide traffic into the cluster.
- External Secret: To handle MYSQL database credentials.

<img src="images/image-2.png">

<img src="images/image-1.png">

<img src="images/image-3.png">

Connect to argocd

<img src="images/image-5.png">

<img src="images/image-4.png">



Setup AWS Secret Manager to store database credentials which will be used by external secret application to provide secret for our mysql database


<img src="images/image-12.png">

<img src="images/image-24.png">



Deployed ClusterSecretStore and ExternalSecret that provide a user-friendly abstraction for the external API that stores and manages the lifecycle of our MYSQL credentials. 

<img src="images/image-25.png">

<img src="images/image-26.png">

<img src="images/image-27.png">

<img src="images/image-38.png">



Update the [yml file](https://gitlab.com/Taiwolawal/cd-task/-/blob/main/carbon-app.yml?ref_type=heads) to make reference to the secret


<img src="images/image-30.png">



Copy the load balancer value and update Route 53 with the subdomain you want to connect to the application

<img src="images/image-8.png">



Update the ingress with the subdomain name

<img src="images/image-9.png">



Use ArgoCD to deploy the application by specifying the repository where the kubernetes manifest file is 

<img src="images/image-10.png">

<img src="images/image-21.png">

<img src="images/image-14.png">



To confirm we have our database credentials in our flask application

<img src="images/image-31.png">

<img src="images/image-37.png">



Creating helm chart, visit [cd-repo](https://gitlab.com/Taiwolawal/cd-task) for the helm chart

<img src="images/image-13.png">



Remove everything in side templates

````
cd carbon-app-helm
rm -rf templates/*
touch templates/carbon-deployment.yaml
vi values.yaml
````

update values.yaml


<img src="images/image-33.png">



Reference the values in yaml file

<img src="images/image-32.png">

<img src="images/image-34.png">

<img src="images/image-35.png">

<img src="images/image-36.png">

<img src="images/image-18.png">

<img src="images/image-19.png">



